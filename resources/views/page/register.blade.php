<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Register</title>
</head>
<body>
    <h1> Buat Account Baru! </h1>
        <h3> Sign Up Form </h3>
            <form action="/welcome", method="post">
                @csrf
                <p> First name: </p>
                <label for="fname"></label>
                    <input type="text" name="fname"><br>
                
                <p> Last name: </p>
                <label for="lname"></label>
                    <input type="text" name="lname"><br>

                <p> Gender: </p>
                <label>
                    <input type="radio" name="gender" value="Laki-laki"> Male <br>
                    <input type="radio" name="gender" value="Perempuan"> Female <br>
                    <input type="radio" name="gender" value="Lainnya"> Other
                </label>

                <p> Nationality: </p>
                <select>
                    <option name="nationality" value="Indonesia"> Indonesian </option>
                    <option name="nationality" value="Malaysia"> Malaysian </option>
                    <option name="nationality" value="Inggris"> British </option>
                    <option name="nationality" value="Amerika"> American </option>
                </select>

                <p> Language Spoken: </p>
                <input type="checkbox" name="language" value="Indonesia"> Bahasa Indonesia <br>
                <input type="checkbox" name="language" value="Inggris"> English <br>
                <input type="checkbox" name="language" value="Lainnya"> Other

                <p> Bio: </p>
                    <textarea name="komen" cols="30" rows="10"></textarea> <br>
                    <a href="/welcome">
                        <input type="submit" value="Sign Up">
                    </a>
            </form>
</body>
</html>