<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    //create
    public function create(){
        return view('cast.create');
    }

    //store
    public function store(Request $request){
        $request->validate([
            'nama' => 'required|max:45',
            'umur' => 'required|max:11',
            'bio'  => 'required',
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }

    //index
    public function index(){
        $cast = DB::table('cast')->get();
        return view('cast.index', compact('cast'));
    }

    //show
    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('cast'));
    }
    
    //edit
    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }

    //update
    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required|max:45',
            'umur' => 'required|max:11',
            'bio'  => 'required',
        ]);

        $query = DB::table('cast')
              ->where('id', $id)
              ->update([
                  'nama' => $request['nama'],
                  'umur' => $request['umur'],
                  'bio' => $request['bio']
                ]);

        return redirect('/cast');
    }

    //delete
    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
