<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome</title>
</head>
<body>
    <h1> SELAMAT DATANG! {{$fnama}} {{$lnama}}</h1>
    <h3> Terima kasih telah bergabung di Website Kami. Media Belajar Kita Bersama!</h3>
    <h4> Berikut Informasi Data Diri Peserta:</h4>
    Nama Lengkap : {{$fnama}} {{$lnama}} <br><br>
    Jenis Kelamin : {{$kelamin}} <br><br>
    Kewarganegaraan : {{$warga}} <br><br>
    Bahasa : {{$bahasa}} <br><br>
    Komentar : {{$komentar}} 

</body>
</html>