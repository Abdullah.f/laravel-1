@extends('layout.master')

@section('judul')
    Table Cast
@endsection

@section('konten')
<a href="/cast/create" class="btn btn-success btn-sm mb-2">Tambah Data</a>
    <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Age</th>
            <th scope="col">Bio</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($cast as $key => $item)
              <tr>
                  <td>{{$key + 1}}</td>
                  <td>{{$item->nama}}</td>
                  <td>{{$item->umur}}</td>
                  <td>{{$item->bio}}</td>
                  <td>
                    
                    <form action="/cast/{{$item->id}}" method="POST">
                        <a href="/cast/{{$item->id}}" class="btn btn-primary btn-sm">Details</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        @method('delete')
                        @csrf
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                  </td>
              </tr>
          @empty
              <tr>
                  <td>Data Masih Kosong</td>
              </tr>
          @endforelse
        </tbody>
      </table>
@endsection