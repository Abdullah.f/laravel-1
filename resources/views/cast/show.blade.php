@extends('layout.master')

@section('judul')
    Details Cast {{$cast->nama}}
@endsection

@section('konten')
    <h3>{{$cast->nama}}</h3>
    <h5>{{$cast->umur}}</h5>
    <br>
    <h5>Deskripsi</h5>
    <p>{{$cast->bio}}</p>
@endsection