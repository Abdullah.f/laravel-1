<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('page.register');
    }

    public function show(Request $request){
        // dd($request->all());
        $fnama = $request->fname;
        $lnama = $request->lname;
        $kelamin = $request->gender;
        $warga = $request->nationality;
        $bahasa = $request->language;
        $komentar = $request->komen;
        return view('page.welcome',
        compact('fnama', 'lnama', 'kelamin', 'warga', 'bahasa', 'komentar'));
    }
}
